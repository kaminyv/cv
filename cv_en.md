# Yan Kaminsky

**Backend Developer**

### Contacts

Email: [kaminyv@gmail.com](mailto:kaminyv@gmail.com) (preferred method of contact)

Telegram: [@kaminyv](https://t.me/kaminyv)

Linkedin: [kaminyv](https://www.linkedin.com/in/kaminyv)

Skype: [kaminyv](skype:kaminyv?chat)

Phone: [+79649858944](tel:+79649858944)

## Experience

### TrendUp
*March 2023 - Present*

**Backend Developer**

* Participated in the development and maintenance of an LMS (Learning Management System).
* Adapted the system architecture for high loads.
* Developed microservices for analytics collection and interaction with external systems.
* More will be added...

**Stack:** PHP, CMS Wordpress, Python, Go, JS, HTML, CSS, Bitrix24

---

### DemandBase Inc.
*May 2021 — January 2023 (1 year 9 months)*

**Developer Analyst**

* Analyzed integration of web technologies - reviewed over 3500 technologies.
* Created instructions and use cases for systems to gather information and identify technologies.
* Developed Python application and service prototypes: automatic detection of advertising technologies on websites, scanning websites to determine network information. Using Flask, Django, Selenium, Beautifulsoup, Asyncio.
* Gathered and aggregated data from various websites, developed ETL prototypes using Luigi.


**Stack:** Python, Django, Flask, Asyncio, Beautifulsoup, Linux, Google Workspace, Docker, Git, JavaScript, HTML, CSS

---

### Bogatovsky Municipal District Administration
*September 2018 — March 2021 (2 years 7 months)*

**Developer**

* Created an automated internal document management system in Python (Django).
* Enhanced and automated interactions with GIS systems.
* Developed a new version of the website using PHP (Laravel) and responsive design.
* Organized administration process of Microsoft Hyper-V virtual machines.
* Developed an inventory system for the information support department using Python (Django).

**Stack:** Python, PHP, Django, Django Rest Framework, Pytest, Laravel, JavaScript, Gulp, HTML, CSS,  Bootstrap, jQuery, Microsoft Windows Server, Microsoft Hyper-V, Git

---

### Private Practice / Freelance
*October 2016 — September 2018 (2 years)*

**Developer**

* Developed projects using Django and Laravel frameworks.
* Created and maintained over 10 web projects of varying complexity with different stacks.
* Developed WordPress/OpenCart plugins.
* Developed and integrated with payment systems.

**Stack:** Python, Django, PHP, Laravel, WordPress, WooCommerce, OpenCart, HTML, CSS, JavaScript, Bootstrap, jQuery

---

### Transport Card
*June 2013 — August 2015 (2 years 3 months)*

**Developer**

* Administered and developed an analytical SQL database on FireBird.
* Automated creation of analytical reports.
* Created an application interface for aggregation and report generation.
* Implemented a new engine for the corporate website using PHP. Developed a user portal for the transportation application.
* Improved client-server interaction for 1C by 9 times.

**Stack:** SQL, FireBired, PHP, HTML, CSS, JavaScript, Bootstrap, jQuery, 1C

---

## Key Skills

* Python, Django Framework, Django Rest Framework, Flask, Luigi, Asyncio, Selenium, Beautifulsoup
* PHP, Laravel, CakePHP, Yii, CMS Wordpress, OpenCart
* SQL, MySQL, PostgreSQL
* JavaScript, jQuery, Gulp
* HTML, CSS, Bootstrap
* Linux, Bash, Windows Server, Hyper-V, Docker, Git
* Google Workspace, Atlassian Jira, Atlassian Confluence
* Stress Resistance
* English — A1 — Beginner

## About Me

A programmer with a passion for web development, data collection, and process automation.
Dedicates time to the best programming and design practices.
Always aims to find optimal solutions and gladly takes on "impossible" tasks.

## Education

### Ural Technological College
*Secondary Special Education / 2006*

**Faculty:** Computer Engineering

**Specialization:** Automated Information Processing and Control Systems

## Recommendations

### TrendUp
Sergey Kiselev (Tech Lead)

### DemandBase Inc. 
Ivan Lozitsky (Data Sourcing Lead)
